'''
pyfuscate

Cross version obfuscation can work like this::

    python2 -c '__builtins__.eval(__builtins__.compile("import os ; print(os.listdir(\"/home\"))", "bar", "exec"))'

'''
import os
import random
from hashlib import sha256
from codecs import encode, decode
from string import ascii_letters, digits

random_id = None


def random_alphanum():
    return random.choice(ascii_letters + digits)


def random_space(mn=0, mx=3):
    return ' ' * random.randint(mn, mx)


def random_bytes(mn=3, mx=20):
    return os.urandom(random.randint(mn, mx))


def random_base64(mn=3, mx=20):
    bs = random_bytes(mn=mn, mx=mx)
    return encode(bs, 'base64').strip().decode('utf8')


def random_hex(mn=3, mx=20):
    bs = random_bytes(mn=mn, mx=mx)
    return encode(bs, 'hex').strip().decode('utf8')


def wrap_quote(s):
    return '{quote}{s}{quote}'.format(
        quote=random.choice('\'"'),
        s=s,
    )


def space_assign(left, right, mn=0, mx=3):
    return '{left}{space1}={space2}{right}'.format(
        left=left,
        right=right,
        space1=random_space(mn=mn, mx=mx),
        space2=random_space(mn=mn, mx=mx),
    )


def random_expr():
    case = random.randint(1, 5)
    if case == 1:
        return wrap_quote('{space1}{id}{space2}'.format(
            space1=random_space(mx=1),
            id=random_id(),
            space2=random_space(mx=1),
        ))
    elif case == 2:
        return str(random.randint(0, 100000))
    elif case == 3:
        return hex(random.randint(0, 100000))
    elif case == 4:
        return wrap_quote(random_base64())
    elif case == 5:
        return wrap_quote(random_hex())


def garbage_assign():
    return '{id}{space1}={space2}{assign}'.format(
        id=random_id(),
        space1=random_space(mx=1),
        space2=random_space(mx=1),
        assign=random_expr(),
    )


def random_int(i):
    r = random.uniform(0, 1)
    if r < 0.5:
        return str(i)
    elif r < 0.8:
        return hex(i)
    elif r < 0.9:
        return bin(i)
    else:
        return oct(i)


def split_string(s):
    ints = [ord(x) for x in s]
    xored = []
    for i in ints:
        r1 = random.randint(1, 10000)
        if random.randint(1, 2) == 2:
            new = '{i}^{r1}'.format(i=random_int(i ^ r1), r1=random_int(r1))
        else:
            new = '{r1}^{i}'.format(i=random_int(i ^ r1), r1=random_int(r1))
        xored.append(new)
    xored = '[' + ','.join(n for n in xored) + ']'
    return '{quote}{quote}.join([chr({id}) for {id} in {xored}])'.format(
        quote=random.choice('\'"'),
        id=random_id(),
        xored=xored,
    )


def random_getattr(obj, attr):
    return 'getattr({space1}{obj}{space2},{space3}{attr}{space4})'.format(
        obj=obj,
        attr=split_string(attr),
        space1=random_space(mx=2),
        space2=random_space(mx=2),
        space3=random_space(mx=2),
        space4=random_space(mx=2),
    )


def random_import(import_var, module):
    return '{import_var}({space1}{module}{space2})'.format(
        import_var=import_var,
        module=split_string(module),
        space1=random_space(mx=2),
        space2=random_space(mx=2),
    )


def garbage_assign_wrap(s, mn_left=1, mx_left=4, mn_right=1, mx_right=4):
    new = ''
    for i in range(random.randint(mn_left, mx_left)):
        new += garbage_assign() + ';'
    new += s
    for i in range(random.randint(mn_right, mx_right)):
        new += ';' + garbage_assign()
    return new


def init_random_id():
    global random_id
    used = set()

    def _random_id():
        while True:
            ln = random.randint(3, 20)
            first_letter = random.choice(ascii_letters)
            id = first_letter + ''.join(
                random_alphanum() for _ in range(ln)
            )
            if id not in used:
                used.add(id)
                return id

    random_id = _random_id


def encode_sumstr(s):
    l = len(s)
    if l == 1:
        return wrap_quote(s)
    arr = []
    left = s[:]
    while left:
        m = len(left) // 2
        if len(left) == 1 or m == 0:
            arr.append(left)
            break
        i = random.randint(1, m)
        arr.append(left[:i])
        left = left[i:]
    new = wrap_quote(arr[0])
    for i in arr[1:]:
        new = '{new}+{i}'.format(new=new, i=wrap_quote(i))
    return new


def encode_zlib(code, _decode):
    encoded = encode(code.encode('utf8'), 'zlib')
    encoded = encode(encoded, 'base64').decode('utf8').replace('\n', '')
    return (
        '{_decode}('
            '{_decode}('
                '{code},'
                '{_b64}'
            '),'
            '{_zlib}'
        ').decode({_utf8})'
    ).format(
        _b64=split_string('base64'),
        _zlib=split_string('zlib'),
        _utf8=split_string('utf8'),
        _decode=_decode,
        code='b' + wrap_quote(encoded),
    )


def obfuscate(code, zlib=False, anti_analysis=False, filename=None):
    init_random_id()
    builtins = random_id()
    _eval = random_id()
    _compile = random_id()
    _fname = random_base64(mn=10, mx=20)
    _exec = split_string('exec')
    _decode = random_id()

    s = space_assign(builtins, '__builtins__')
    s = garbage_assign_wrap(s)
    s += '; ' + space_assign(_eval, random_getattr(builtins, 'eval'))
    s = garbage_assign_wrap(s)
    s += '; ' + space_assign(_compile, random_getattr(builtins, 'compile'))
    encoded = split_string(code)
    if zlib:
        s += '; from codecs import decode as {_decode}'.format(
            _decode=_decode,
        )
        encoded = encode_zlib(code, _decode)
    s += (
        '; {_eval}({space1}{_compile}({space1}{code}{space2},{space3}{quote}'
        '{_fname}{quote}{space4},{space5}{_exec}{space6})'
        '{space6})'
    ).format(
        code=encoded,
        _eval=_eval,
        _compile=_compile,
        _fname=_fname,
        _exec=_exec,
        quote=random.choice('\'"'),
        space1=random_space(mx=1),
        space2=random_space(mx=1),
        space3=random_space(mx=1),
        space4=random_space(mx=1),
        space5=random_space(mx=1),
        space6=random_space(mx=1),
    )
    if not anti_analysis:
        return s
    sha = sha256(s.encode('utf8')).hexdigest()
    import_var = random_id()
    inspect_var = random_id()
    cf_var = random_id()
    fi_var = random_id()
    sys_var = random_id()
    start = space_assign(import_var, '__import__') + ';'
    start += space_assign(
        sys_var,
        random_import(import_var, 'sys')
    ) + ';'
    start += space_assign(
        inspect_var,
        random_import(import_var, 'inspect')
    ) + ';'
    start += space_assign(
        cf_var,
        random_getattr(inspect_var, 'currentframe')
    ) + '();'
    start += space_assign(
        fi_var,
        random_getattr(inspect_var, 'getframeinfo')
    ) + '({});'.format(cf_var)
    start += '{fi_filename}=={start_str} or {sysexit}();'.format(
        fi_filename=random_getattr(fi_var, 'filename'),
        start_str=split_string(filename or '<string>'),
        sysexit=random_getattr(sys_var, 'exit'),
    )
    start += '{fi_lineno}=={one} or {sysexit}();'.format(
        fi_lineno=random_getattr(fi_var, 'lineno'),
        one=random.choice([1, '0b01', '0o001', '0x01', '0+1', 'len("a")']),
        sysexit=random_getattr(sys_var, 'exit'),
    )
    return start + s
