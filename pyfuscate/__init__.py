'''
pyfuscate

obfuscate python code
'''
import sys
from .obf import obfuscate

__title__ = 'pyfuscate'
__version__ = '0.1.0'
__all__ = ('obfuscate',)
__author__ = 'Johan Nestaas <johannestaas@gmail.com>'
__license__ = 'GPLv3+'
__copyright__ = 'Copyright 2018 Johan Nestaas'


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('input', nargs='?')
    parser.add_argument('--bash', '-b', action='store_true')
    parser.add_argument('--zlib', '-z', action='store_true')
    parser.add_argument('--anti-analysis', '--aa', '-a', action='store_true')
    parser.add_argument('--aa-filename', '--filename', '-f', nargs='?')
    args = parser.parse_args()
    if not args.input or args.input == '-':
        code = sys.stdin.read()
    else:
        with open(args.input) as f:
            code = f.read()
    obfuscated = obfuscate(
        code, zlib=args.zlib, anti_analysis=args.anti_analysis,
        filename=args.aa_filename,
    )
    if not args.bash:
        print(obfuscated)
    else:
        print('python -c \'{obfuscated}\''.format(
            obfuscated=obfuscated.replace("'", "'\"'\"'"),
        ))


if __name__ == '__main__':
    main()
