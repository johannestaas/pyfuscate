pyfuscate
=========

obfuscate python code

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Simply run it::

    $ pyfuscate test.py
    $ cat test.py | pyfuscate

Output the obfuscated python as a bash command::

    $ pyfuscate test.py --bash
    python -c '...'

Use --help/-h to view info on the arguments::

    $ pyfuscate --help

Release Notes
-------------

:0.1.0:
    Works
:0.0.1:
    Project created
